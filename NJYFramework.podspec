#
# Be sure to run `pod lib lint AipOCRSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name      = 'NJYFramework'
    s.version   = '1.0'
    s.summary   = 'NJYFramework.'
    s.homepage  = 'https://gitlab.com/rogerkuu/openframework'
    s.license   = 'Apache License, Version 2.0'
    s.author    = { "mjgu" => "mjgu@lpht.com.cn" }
    s.source    = { :git => 'https://gitlab.com/rogerkuu/openframework.git', :tag => s.version.to_s }

    s.platform                = :ios
    s.ios.deployment_target   = '9.0'

    s.ios.deployment_target = '9.0'
    s.subspec 'openframework' do |b|
      b.vendored_frameworks ='njy/openframework.framework'
    end

    s.dependency 'ReactiveCocoa', '~> 7.0'
    s.dependency 'ReactiveSwift', '~> 3.0'
    s.dependency 'Moya/ReactiveSwift', '~> 11.0'
    s.dependency 'ObjectMapper', '~> 3.4'
    s.dependency 'Kingfisher', '~> 4.6.4'
    s.dependency 'SnapKit', '~> 4.0.0'
    s.dependency 'IQKeyboardManagerSwift'
    s.dependency 'SwiftyJSON'
    s.dependency 'KeychainAccess'
    s.dependency 'Socket.IO-Client-Swift', '~> 13.1.0'
    s.dependency 'NVActivityIndicatorView'
    s.dependency 'BMPlayer', '~> 1.0.0'
    s.dependency 'NextGrowingTextView', '~> 1.3.0'
    s.dependency 'Cosmos', '~> 16.0'
    s.dependency 'Kanna'#, '~> 4.0.0'
    s.dependency 'SugarRecord'
    s.dependency 'SKPhotoBrowser', '~> 5.1.0'
    s.dependency 'MarqueeLabel/Swift','~> 3.1.6' 
    s.dependency 'SwiftMessages', '~> 5.0.1'
    s.dependency 'SensorsAnalyticsSDK', '~> 1.10.26'  #神策统计
    s.dependency 'YNSearch'
    s.dependency 'Hero'
    s.dependency 'SVProgressHUD'
    s.dependency 'TZImagePickerController'
    s.dependency 'MJRefresh'
    s.dependency 'AliyunOSSiOS'
    s.dependency 'RealReachability'
    s.dependency 'YYText'
    s.dependency 'UITextView+Placeholder'
    s.dependency 'ZYCornerRadius', '~> 1.0.2'
    s.dependency 'YBPopupMenu', '~> 1.0.0'
    s.dependency 'UICollectionViewLeftAlignedLayout'
    s.dependency 'pop'
    s.dependency 'XHRealTimeBlur'
    s.dependency 'BEMCheckBox'
    s.dependency 'M13ProgressSuite'
end