//
//  openframework.h
//  openframework
//
//  Created by lyn on 2019/6/21.
//  Copyright © 2019 LPMAS. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for openframework.
FOUNDATION_EXPORT double openframeworkVersionNumber;

//! Project version string for openframework.
FOUNDATION_EXPORT const unsigned char openframeworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <openframework/PublicHeader.h>

#import <CommonCrypto/CommonHMAC.h>
//获取ip
#include <ifaddrs.h>

//拍照、获取照片相关
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import <CommonCrypto/CommonDigest.h>

#import "SimulateIDFA.h"
#import "PopMenu.h"
#import "NNValidationCodeView.h"
